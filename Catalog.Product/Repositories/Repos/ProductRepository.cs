﻿using Catalog.Product.Data;
using Catalog.Product.DTO;
using Catalog.Product.Models;
using Catalog.Product.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Product.Repositories.Repos
{
    public class ProductRepository : IProductRepository
    {
        private readonly CatalogContext _context;

        public ProductRepository(CatalogContext context)
        {
            _context = context;
        }

        public async Task<ProductDTO> Add(ProductDTO product)
        {
            Products prod = new Products() { Cost = product.Cost, Id = product.Id, Image = product.Image, Name = product.Name, Price = product.Price };
            _context.Products.Add(prod);
            await _context.SaveChangesAsync();

            return product;
        }

        public async Task<ProductDTO> Delete(int Id)
        {
            var prod = await _context.Products.FindAsync(Id);
            if (prod == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Products.Remove(prod);
            await _context.SaveChangesAsync();

            return new ProductDTO() { Cost = prod.Cost, Id = prod.Id, Image = prod.Image, Name = prod.Name, Price = prod.Price };
        }

        public async Task<ProductDTO> GetProduct(int Id)
        {
            var prod = await _context.Products.FindAsync(Id);

            if (prod == null)
            {
                throw new KeyNotFoundException();
            }

            return new ProductDTO() { Cost = prod.Cost, Id = prod.Id, Image = prod.Image, Name = prod.Name, Price = prod.Price };
        }

        public async Task<List<ProductDTO>> ListProduct()
        {
            List<ProductDTO> result = new List<ProductDTO>();
            List<Products> products = await _context.Products.ToListAsync();
            foreach (Products prod in products)
                result.Add(new ProductDTO() { Cost = prod.Cost, Id = prod.Id, Image = prod.Image, Name = prod.Name, Price = prod.Price });
            
            return result;
        }

        public async Task<ProductDTO> Update(ProductDTO productdto)
        {
            Products product  =await _context.Products.FindAsync(productdto.Id);

            product.Cost = productdto.Cost;
            product.Image = productdto.Image;
            product.Name = productdto.Name;
            product.Price = productdto.Price;
            try
            {
                await _context.SaveChangesAsync();
                return productdto;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (product == null)
                {
                    throw new KeyNotFoundException();
                }
                else
                {
                    throw;
                }
            }

        }
    }
}