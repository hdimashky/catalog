﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Product.DTO;
using Catalog.Product.Models;

namespace Catalog.Product.Repositories.Interfaces
{
    public interface IProductRepository
    {
        Task<List<ProductDTO>> ListProduct();

        Task<ProductDTO> Add(ProductDTO product);

        Task<ProductDTO> Update(ProductDTO product);

        Task<ProductDTO> Delete(int Id);

        Task<ProductDTO> GetProduct(int Id);
    }
}
