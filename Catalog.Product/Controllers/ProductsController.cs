﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Catalog.Product.Services.Interfaces;
using Catalog.Product.DTO;
using System.Net;
using System.Web.Http;
using MassTransit;
using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;
using Catalog.Product.Services.Services;

namespace Catalog.Product.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly IProductProducer _messagePublisher;

        public ProductsController(IProductService service , IProductProducer messagePublisher)
        {
            _service = service;
            _messagePublisher = messagePublisher;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts()
        {
            return await _service.ListProduct();

        }

        [Microsoft.AspNetCore.Mvc.HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProducts(int id)
        {
            try
            {
                return await _service.GetProduct(id);

            }
            catch (KeyNotFoundException ex)
            {
                return NotFound();
            }

        }

        [Microsoft.AspNetCore.Mvc.HttpPut("{id}")]
        public async Task<ActionResult<ProductDTO>> PutProducts(int id, ProductDTO products)
        {
            if (id != products.Id)
            {
                return BadRequest();
            }
            try
            {
                return await _service.Update(products);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

   
        [Microsoft.AspNetCore.Mvc.HttpPost]
        public async Task<ActionResult<ProductDTO>> PostProducts(ProductDTO products)
        {
            ProductDTO prod=  await _service.Add(products);
            _messagePublisher.SendProducts(products);
            return Ok();
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("{id}")]
        public async Task<ActionResult<ProductDTO>> DeleteProducts(int id)
        {
            try
            {
                return await _service.Delete(id);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound();
            }
        }

    }
}
