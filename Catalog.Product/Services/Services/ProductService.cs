﻿using Catalog.Product.DTO;
using Catalog.Product.Repositories.Interfaces;
using Catalog.Product.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Product.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _ProductRepo;

        public ProductService(IProductRepository ProductRepo)
        {
            _ProductRepo = ProductRepo;
        }
        public Task<ProductDTO> Add(ProductDTO product)
        {
            return _ProductRepo.Add(product);
        }

        public Task<ProductDTO> Delete(int Id)
        {
            return _ProductRepo.Delete(Id);
        }

        public Task<ProductDTO> GetProduct(int Id)
        {
            return _ProductRepo.GetProduct(Id);
        }

        public Task<List<ProductDTO>> ListProduct()
        {
            return _ProductRepo.ListProduct();
        }

        public Task<ProductDTO> Update(ProductDTO product)
        {
            return _ProductRepo.Update(product);
        }
    }
}
