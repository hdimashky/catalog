﻿using Catalog.Product.DTO;
using Catalog.Product.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Product.Services.Interfaces
{
    public interface IProductProducer
    {
        void SendProducts(ProductDTO customer);
    }
}
