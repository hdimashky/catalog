﻿
using Catalog.Product.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Product.Services.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDTO>> ListProduct();

        Task<ProductDTO> Add(ProductDTO product);

        Task<ProductDTO> Update(ProductDTO product);

        Task<ProductDTO> Delete(int Id);

        Task<ProductDTO> GetProduct(int Id);
    }
}
