﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Product.Services.Interfaces
{
    public interface IMessageProducer
    {
        void SendMessage<T>(T message);
    }
}
