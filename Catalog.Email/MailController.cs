﻿using Catalog.Email.Model;
using Catalog.Email.Services;
using Catalog.Product.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Email
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly IMailService mailService;
        public MailController(IMailService mailService)
        {
            this.mailService = mailService;
        }
        [HttpPost("send")]
        public async Task<IActionResult> SendMail([FromBody] ProductDTO request)
        {
            try
            {
                await mailService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        [HttpGet("random")]
        public async Task<IActionResult> random()
        {
            try
            {
                
                return Ok(MailService.GenerateRandomMail());
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        
    }
}
