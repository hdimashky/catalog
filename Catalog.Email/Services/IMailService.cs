﻿using Catalog.Email.Model;
using Catalog.Product.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Email.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(ProductDTO mailRequest);
    }
}
