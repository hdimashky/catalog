﻿using Catalog.Email.Model;
using Catalog.Product.DTO;
using MailKit.Net.Smtp;
using MailKit.Security;
using MassTransit;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Email.Services
{
    public class MailService : IMailService
    {
        private readonly MailSetting _mailSettings;
        public MailService(IOptions<MailSetting> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendEmailAsync(ProductDTO product)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(GenerateRandomMail()));
            email.Subject = "Product: " + product.Name + " Added";
            var builder = new BodyBuilder();
            if (product.Image != null)
            {
                byte[] bytes = Convert.FromBase64String(product.Image);
                builder.Attachments.Add(product.Name + "_Image.png", bytes, ContentType.Parse("image/x-png"));
            }
            builder.HtmlBody = "Product: " + product.Name + " with cost: " + product.Cost + ", price: " + product.Price + " was added";
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
            try
            {
                await smtp.SendAsync(email);
            }catch(Exception ex)
            {

            }
            smtp.Disconnect(true);
        }

        public static string GenerateRandomMail()
        {
            String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.Length < 5)
            { // length of the random string.
                int index = (int)(rnd.NextDouble() * SALTCHARS.Length);
                salt.Append(SALTCHARS[index]);
            }
            String saltStr = salt.ToString();
            return saltStr;
        }

    }

}
